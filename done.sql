/*
Created		2017-01-21
Modified		2017-02-04
Project
Model
Company
Author
Version
Database		mySQL 5
*/


Create table user (
	user_id Varchar(50) NOT NULL,
	userGroup_code Int NOT NULL,
	user_pw Varchar(100) NOT NULL,
 Primary Key (user_id)) ENGINE = InnoDB;

Create table student (
	user_id Varchar(50) NOT NULL,
	student_number Int NOT NULL,
	student_name Varchar(100) NOT NULL,
	student_phone Char(13) NOT NULL,
	student_email Varchar(255) NOT NULL,
	student_major Varchar(50) NOT NULL,
	student_grade Char(1) NOT NULL,
	student_tutor Varchar(100),
	student_sex Char(1) NOT NULL,
 Primary Key (student_number)) ENGINE = InnoDB;

Create table poster (
	user_id Varchar(50) NOT NULL,
	posterState_code Int NOT NULL,
	category_code Int NOT NULL,
	poster_id Int NOT NULL AUTO_INCREMENT,
	poster_title Varchar(255) NOT NULL,
	poster_startdate Datetime NOT NULL,
	poster_regidate Datetime NOT NULL,
	poster_enddate Datetime NOT NULL,
	poster_contents Text NOT NULL,
	poster_limitcount Int NOT NULL,
 Primary Key (poster_id)) ENGINE = InnoDB;

Create table statistics (
	statistics_id Int NOT NULL AUTO_INCREMENT,
 Primary Key (statistics_id)) ENGINE = InnoDB;

Create table applyList (
	poster_id Int NOT NULL,
	applyListState_code Int NOT NULL,
	student_number Int NOT NULL,
	applyList_regidate Datetime NOT NULL,
	applyList_canceldate Datetime,
	applyList_approval Bool,
	applyList_dropdate Datetime,
 Primary Key (poster_id,student_number)) ENGINE = InnoDB;

Create table questionList (
	poster_id Int NOT NULL,
	student_number Int NOT NULL,
	question_content Text NOT NULL,
	question_date Datetime NOT NULL,
	question_secret Bool,
 Primary Key (poster_id,student_number)) ENGINE = InnoDB;

Create table category (
	category_code Int NOT NULL,
	category_name Varchar(150) NOT NULL,
 Primary Key (category_code)) ENGINE = InnoDB;

Create table userGroup (
	userGroup_code Int NOT NULL,
	userGroup_name Varchar(255) NOT NULL,
	userGroup_subname Varchar(255),
 Primary Key (userGroup_code)) ENGINE = InnoDB;

Create table qAnswer (
	student_number Int NOT NULL,
	poster_id Int NOT NULL,
	qAnswer_date Datetime NOT NULL,
	qAnswer_writer Varchar(100) NOT NULL,
 Primary Key (student_number,poster_id)) ENGINE = InnoDB;

Create table privateCheck (
	poster_id Int NOT NULL,
	student_number Int NOT NULL,
	privateCheck_form Text NOT NULL,
	privateCheck_regidate Datetime NOT NULL,
	privateCheck_agree Bool NOT NULL,
 Primary Key (poster_id,student_number)) ENGINE = InnoDB;

Create table posterState (
	posterState_code Int NOT NULL,
	posterState_name Varchar(30) NOT NULL,
 Primary Key (posterState_code)) ENGINE = InnoDB;

Create table applyListState (
	applyListState_code Int NOT NULL,
	applyListState_name Varchar(30) NOT NULL,
 Primary Key (applyListState_code)) ENGINE = InnoDB;

Create table messageList (
	message_id Int NOT NULL AUTO_INCREMENT,
	poster_id Int NOT NULL,
	message_date Datetime NOT NULL,
	message_content Text NOT NULL,
 Primary Key (message_id)) ENGINE = InnoDB;

Create table mStudentList (
	message_id Int NOT NULL,
	student_number Int NOT NULL,
	msl_tempid Int NOT NULL,
	msl_readdate Datetime,
 Primary Key (message_id,student_number)) ENGINE = InnoDB;


Alter table poster add Foreign Key (user_id) references user (user_id) on delete no action on update cascade;
Alter table student add Foreign Key (user_id) references user (user_id) on delete no action on update cascade;
Alter table applyList add Foreign Key (student_number) references student (student_number) on delete cascade on update cascade;
Alter table questionList add Foreign Key (student_number) references student (student_number) on delete cascade on update cascade;
Alter table mStudentList add Foreign Key (student_number) references student (student_number) on delete cascade on update cascade;
Alter table applyList add Foreign Key (poster_id) references poster (poster_id) on delete cascade on update cascade;
Alter table questionList add Foreign Key (poster_id) references poster (poster_id) on delete cascade on update cascade;
Alter table messageList add Foreign Key (poster_id) references poster (poster_id) on delete cascade on update cascade;
Alter table privateCheck add Foreign Key (poster_id,student_number) references applyList (poster_id,student_number) on delete cascade on update cascade;
Alter table qAnswer add Foreign Key (poster_id,student_number) references questionList (poster_id,student_number) on delete cascade on update cascade;
Alter table poster add Foreign Key (category_code) references category (category_code) on delete no action on update cascade;
Alter table user add Foreign Key (userGroup_code) references userGroup (userGroup_code) on delete no action on update cascade;
Alter table poster add Foreign Key (posterState_code) references posterState (posterState_code) on delete no action on update cascade;
Alter table applyList add Foreign Key (applyListState_code) references applyListState (applyListState_code) on delete no action on update cascade;
Alter table mStudentList add Foreign Key (message_id) references messageList (message_id) on delete cascade on update cascade;
